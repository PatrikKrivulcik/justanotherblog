<?php

namespace App\Controller;

use App\Entity\Post;
use App\Service\Stats;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("", name="index.")
 */
class IndexController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    public $em;

    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
    }
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        // get banner article
        $banner = $this->em->getRepository(Post::class)->getBanner();
        $banner = [
            'id' => $banner->getId(),
            'title' => $banner->getTitle(),
            'excerpt' => $banner->getExcerpt(),
            'cover' => $_ENV['ASSET_URL'] . $_ENV['COVER_UPLOAD_PATH_PUBLIC'] . $banner->getCover(),
            'created' => $banner->getCreated()
        ];

        // get featured articles
        $featured = $this->em->getRepository(Post::class)->getFeatured();
        $featured = array_map(function (Post $i) {
            return [
                'id' => $i->getId(),
                'title' => $i->getTitle(),
                'excerpt' => $i->getExcerpt(),
                'cover' => $_ENV['ASSET_URL'] . $_ENV['COVER_UPLOAD_PATH_PUBLIC'] . $i->getCover(),
            ];
        }, $featured);
        return $this->render('web/index.html.twig', [
            'banner' => $banner,
            'featured' => $featured
        ]);
    }
    /**
     * @Route("/posts/{filter<new|best>?}/{page<\d+>?1}", name="posts")
     */
    public function posts($filter, $page): Response
    {
        // force filter type in url
        if ($filter == null) {
            return $this->redirectToRoute('index.posts', ['filter' => 'new']);
        }

        $posts = $this->em->getRepository(Post::class)->getPage($page, $filter);

        // redirect if page empty
        if (count($posts) == 0) {
            return $this->redirectToRoute('index.posts', [
                'filter' => $filter,
                'page' => 1
            ]);
        }

        // map posts to readable array
        $posts = array_map(function (Post $i) {
            return [
                'id' => $i->getId(),
                'title' => $i->getTitle(),
                'excerpt' => $i->getExcerpt(),
                'created' => $i->getCreated(),
                'cover' => $_ENV['ASSET_URL'] . $_ENV['COVER_UPLOAD_PATH_PUBLIC'] . $i->getCover(),

            ];
        }, $posts);



        return $this->render('web/posts.html.twig', [
            'posts' => $posts
        ]);
    }
    /**
     * @Route("/posts/{id<\d+>}", name="post")
     */
    public function post($id, Stats $stats, Request $req): Response
    {
        $post = $this->em->getRepository(Post::class)->find($id);
        if ($post == null) {
            throw $this->createNotFoundException();
        }

        // stats
        $stats->captureReq($req);

        $post = [
            'id' => $post->getId(),
            'title' => $post->getTitle(),
            'excerpt' => $post->getExcerpt(),
            'created' => $post->getCreated(),
            'body' => $post->getBody(),
            'cover' => $_ENV['ASSET_URL'] . $_ENV['COVER_UPLOAD_PATH_PUBLIC'] . $post->getCover(),
            'author' => [
                'id' => $post->getAuthor()->getId(),
                'name' => $post->getAuthor()->getName(),
                'picture' => $_ENV['ASSET_URL'] . $_ENV['AUTHOR_PROFILE_PATH_PUBLIC'] . $post->getAuthor()->getPicture()
            ]
        ];
        return $this->render('web/post.html.twig', [
            'post' => $post
        ]);
    }


    /**
     * @Route("sitemap.xml", name="sitemap")
     */
    public function sitemap()
    {
        $routes = [
            'index.index',
            'index.posts'
        ];


        $today = date('Y-m-d');
        $data = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"> ';

        // loop through $routes pages
        foreach ($routes as $route) {
            $route = $this->generateUrl($route, [], UrlGeneratorInterface::ABSOLUTE_URL);
            $data .= "<url>
                         <loc>$route</loc>
                        <lastmod>$today</lastmod>
                    </url>";
        }
        // fetch posts
        $posts = $this->em->getRepository(Post::class)->findAll();
        foreach ($posts as $post) {
            $route = $this->generateUrl('index.post', [
                'id' => $post->getId()
            ], UrlGeneratorInterface::ABSOLUTE_URL);
            $data .= "<url>
                         <loc>$route</loc>
                        <lastmod>$today</lastmod>
                    </url>";
        }


        $data = $data . '</urlset>';
        $res = new Response(
            $data,
            200,
            [
                'Content-type' => 'application/xml'
            ]
        );
        return $res;
    }
}
