<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\AuthorType;
use App\Form\PostType;
use App\Service\Stats;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/author", name="author.")
 */
class AuthorController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    public $em;

    public function __construct(EntityManagerInterface $em, Stats $stats)
    {
        $this->em = $em;
        $this->stats = $stats;
    }


    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        return $this->render('author/index.html.twig');
    }


    /**
     * @Route("/posts/write", name="posts.write")
     */
    public function write(Request $request, SluggerInterface $slugger): Response
    {
        // create form
        $form = $this->createForm(PostType::class);

        // handle form submission request
        $form->handleRequest($request);


        // validate form
        if ($form->isSubmitted() && $form->isValid()) {

            //creat post object from form data
            $post = $form->getData();

            // assign post author
            $post->setAuthor($this->getUser());

            // get uploaded file object
            $cover = $form->get('cover')->getData();

            // compute new filename
            $filename = pathinfo($cover->getClientOriginalName(), PATHINFO_FILENAME);

            $filename = $slugger->slug($filename);

            $filename = $filename . '--' . uniqid() . '.' . $cover->guessExtension();

            // set object filename
            $post->setCover($filename);



            // upload file
            $cover->move(
                $this->getParameter('kernel.project_dir') . $_ENV['COVER_UPLOAD_PATH'],
                $filename
            );

            // save post
            $this->em->persist($post);
            $this->em->flush();

            // redirect to all postst

            return $this->redirectToRoute('author.posts');
        }

        return $this->render('author/write.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/posts/{page?1}", name="posts")
     */
    public function posts($page): Response
    {
        // get number of post pages
        $pageCount =  $this->em->getRepository(Post::class)->authorGetPostPages($this->getUser());
        // get all post of current user
        $posts = $this->em->getRepository(Post::class)->getPageByUser($page, $this->getUser());

        if (count($posts)  == 0) {
            return $this->redirectToRoute('author.posts');
        }

        // map posts to readable array
        $posts = array_map(function (Post $post) {
            $views = count($this->stats->getPostData($post->getId()));
            return [
                'id' => $post->getId(),
                'title' => $post->getTitle(),
                'created' => $post->getCreated(),
                'public' => $post->getPublic(),
                'views' => $views
            ];
        }, $posts);

        // render
        return $this->render('author/posts.html.twig', [
            'posts' => $posts,
            'pageCount' => $pageCount
        ]);
    }


    /**
     * @Route("/posts/edit/{id}", name="posts.edit")
     */
    public function edit($id, Request $request, SluggerInterface $slugger, Filesystem $fileSystem)
    {
        $post = $this->em->getRepository(Post::class)->find($id);

        // check ownership
        if ($post->getAuthor() != $this->getUser()) {
            return $this->redirectToRoute('author.posts');
        }

        $form = $this->createForm(PostType::class, $post, ['edit' => true]);



        // handle form
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();

            // handle file change

            if ($form->get('cover')->getData() != null) {
                $cover = $form->get('cover')->getData();

                //delete old file
                $oldpath = $this->getParameter('kernel.project_dir') . $_ENV['COVER_UPLOAD_PATH'] . '/' . $post->getCover();
                $fileSystem->remove($oldpath);

                // compute new filename
                $filename = pathinfo($cover->getClientOriginalName(), PATHINFO_FILENAME);
                $filename = $slugger->slug($filename);
                $filename = $filename . '--' . uniqid() . '.' . $cover->guessExtension();

                // upload new file
                $cover->move(
                    $this->getParameter('kernel.project_dir') . $_ENV['COVER_UPLOAD_PATH'],
                    $filename
                );


                //change value in object
                $post->setCover($filename);
            }
            //save post

            $this->em->persist($post);
            $this->em->flush();

            return $this->redirectToRoute('author.posts');
        }

        return $this->render('author/write.html.twig', [
            'form' => $form->createView(),
            'edit' => true,
            'coverImagePath' => $_ENV['ASSET_URL'] . $_ENV['COVER_UPLOAD_PATH_PUBLIC'] . $post->getCover(),
            'coverImageName' => $post->getCover(),
        ]);
    }


    /**
     * @Route("/settings", name="settings")
     */
    public function authorSettings(Request $request, UserPasswordEncoderInterface $encoder, Filesystem $fileSystem, SluggerInterface $slugger)
    {
        // creawte object insance
        $user = $this->getUser();

        // create form
        $form = $this->createForm(AuthorType::class, $user);


        // form handle submission request
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            // manually change password
            if ($form->get('password')->getData() != null) {

                $newPass =  $form->get('password')->getData();
                $newPass = $encoder->encodePassword($user, $newPass);
                $user->setPassword($newPass);
            }

            // handle profile picture submission
            if ($form->get('profile')->getData() != null) {
                $profile = $form->get('profile')->getData();

                //delete old file
                if($user->getPicture() != null ){
                    $oldpath = $this->getParameter('kernel.project_dir') . $_ENV['AUTHOR_PROFILE_PATH'] . '/' . $user->getPicture();
                    $fileSystem->remove($oldpath);
                }


                // compute new filename
                $filename = pathinfo($profile->getClientOriginalName(), PATHINFO_FILENAME);
                $filename = $slugger->slug($filename);
                $filename = $filename . '--' . uniqid() . '.' . $profile->guessExtension();

                // upload new file
                $profile->move(
                    $this->getParameter('kernel.project_dir') . $_ENV['AUTHOR_PROFILE_PATH'],
                    $filename
                );


                // //change value in object
                $user->setPicture($filename);
            }

            // save user
            $this->em->persist($user);
            $this->em->flush();
        }


        return $this->render('author/settings.html.twig', [
            'form' => $form->createView(),
            'currentPicturePath' => $_ENV['ASSET_URL'] . $_ENV['AUTHOR_PROFILE_PATH_PUBLIC'].$user->getPicture(),
            'currentPicture' => $user->getPicture(),
        ]);
    }
}
