<?php

namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;


class Stats
{
    /**
     * @var HttpClientInterface
     */
    public $client;

    /**
     * @var string Log file location
     */
    public $logfile = __DIR__ . '/../../log.txt';

    /**
     * @var array Fethed data from file
     */
    public $fetchedData = null;


    public function __construct(HttpClientInterface $client, Filesystem $fs)
    {
        $this->client = $client;
        $this->fs = $fs;
    }

    /**
     * Saves request log to file for statistical purposes
     * 
     * @param Request $request 
     * @return 0
     */
    public function captureReq(Request $request)
    {
        // get client ip adress
        $ip = $request->getClientIp() == '::1' ? '' : $request->getClientIp();

        // get info about ip
        $data = $this->parseIP($ip);
        $country = $data['country'];
        $countrCode = $data['countryCode'];
        $ip = $data['query'];

        // get current timestamp
        $time = time();

        // get post id
        $postid = $request->get('id');
        $log = "{$time}~{$ip}~{$postid}~${countrCode}~{$country}";

        //save to file
        $this->saveToLog($log);
        return 0;
    }

    public function parseIP($ip)
    {
        $req = $this->client->request('GET', 'http://ip-api.com/json/' . $ip . '?fields=status,message,country,countryCode,region,regionName,city,zip,lat,lon,timezone,isp,org,as,asname,reverse,mobile,proxy,hosting,query');
        return $req->toArray();
    }

    public function saveToLog($record)
    {
        $this->fs->appendToFile($this->logfile, PHP_EOL . $record);
    }

    /**
     * Fetches posts stats from log file
     * @param int $id Id of the searched post
     * @return array[][]
     */
    public function getPostData(int $id)
    {
        // first time fetching data
        if ($this->fetchedData == null) {
            // get log file content
            $file = file_get_contents($this->logfile);
            $file = explode(PHP_EOL, $file);

            $file = array_map(function ($i) {
                return explode('~', $i);
            }, $file);

            $data = [];
            foreach ($file as $element) {
                $data[$element[2]][] = $element;
            }
            $this->fetchedData = $data;
        }

        return isset($this->fetchedData[$id]) ? $this->fetchedData[$id] : [];
    }
}
