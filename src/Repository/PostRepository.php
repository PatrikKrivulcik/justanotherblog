<?php

namespace App\Repository;

use App\Entity\Post;
use App\Service\Stats;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{

    public $authorPostsPageSize = 3;
    public function __construct(ManagerRegistry $registry, Stats $stats)
    {
        parent::__construct($registry, Post::class);
        $this->stats = $stats;
    }

    /**
     * @return Post Returns a Post object
     */

    public function getBanner()
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.created', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
    /**
     * @return Post[] Returns an array of Post objects
     */

    public function getFeatured()
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.created', 'DESC')
            ->setFirstResult(1)
            ->setMaxResults(6)
            ->getQuery()
            ->getResult();
    }
    /**
     * @return Post[] Returns an array of Post objects
     */

    public function getPage($page, $filter)
    {

        $pagesize = 10;
        if ($filter == 'new') {
            return $this->createQueryBuilder('p')
                ->orderBy('p.created', 'DESC')

                ->getQuery()
                ->getResult();
        } else if ($filter == 'best') {

            $all = $this->createQueryBuilder('p')
                ->orderBy('p.created ', 'DESC')
                ->setFirstResult(($page - 1) * $pagesize)
                ->getQuery()
                ->getResult();
            usort($all, function (Post $first, Post $second) {
                $diffFrist = $first->getCreated()->diff(new \DateTime());
                $diffNumericFirst = $diffFrist->h + 24 * ($diffFrist->d + 30 * ($diffFrist->m + 365 * $diffFrist->y));
                $viewsPerHourFirst = count($this->stats->getPostData($first->getId())) / ($diffNumericFirst + 1);



                $diffSecond = $second->getCreated()->diff(new \DateTime());
                $diffNumericSecond = $diffSecond->h + 24 * ($diffSecond->d + 30 * ($diffSecond->m + 365 * $diffSecond->y)) || 0.001;
                $viewsPerHourSecond = count($this->stats->getPostData($second->getId())) / ($diffNumericSecond + 1);

                if ($viewsPerHourFirst > $viewsPerHourSecond) {
                    return 1;
                } else if ($viewsPerHourFirst < $viewsPerHourSecond) {
                    return -1;
                } else {
                    return 0;
                }
            });
            return $all;
        } else {

            return $this->createQueryBuilder('p')
                ->orderBy('p.created', 'DESC')
                ->setFirstResult(($page - 1) * $pagesize)
                ->setMaxResults($pagesize)
                ->getQuery()
                ->getResult();
        }
    }
    /**
     * @return Post[] Returns an array of Post objects
     */

    public function authorGetPostPages($user)
    {
        $total = (int) $this->createQueryBuilder('p')
            ->select('COUNT(p)')
            ->where('p.author = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();

        return ceil($total / $this->authorPostsPageSize);
    }


    public function getPageByUser($page, $user)
    {
        $pagesize = $this->authorPostsPageSize;
        return $this->createQueryBuilder('p')
            ->where('p.author = :user')
            ->orderBy('p.created', 'DESC')
            ->setParameter('user', $user)
            ->setFirstResult(($page - 1) * $pagesize)
            ->setMaxResults($pagesize)
            ->getQuery()
            ->getResult();
    }

    /*
    public function findOneBySomeField($value): ?Post
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
