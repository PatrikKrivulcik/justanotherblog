<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateUserCommand extends Command
{
    protected static $defaultName = 'app:create-user';

    public function __construct(UserPasswordEncoderInterface  $encoder, EntityManagerInterface $em)
    {
        $this->encoder = $encoder;
        $this->em = $em;

        parent::__construct();
    }
    protected function configure()
    {
        $this
            ->setDescription('Create a user');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // import helper to be able to ask questions
        $helper = $this->getHelper('question');

        // ask for email
        $questionEmail = new Question('<info>New user email [<comment>user@user.com</comment>]:</info> ', 'user@user.com');
        $email = $helper->ask($input, $output, $questionEmail);

        // ask for password
        $questionPassword = new Question('<info>New user password [<comment>123456</comment>]:</info> ', '123456');
        $pass = $helper->ask($input, $output, $questionPassword);

        // creat empty user
        $user = new User();

        // set user email
        $user->setEmail($email);

        // encode password
        $encodedPass = $this->encoder->encodePassword($user, $pass);

        // set user password
        $user->setPassword($encodedPass);

        //save user
        $this->em->persist($user);
        $this->em->flush();

        // log success
        $output->writeln("<info>User <comment>".$email."</comment> has been created.</info>");
        

        // clear cache
        $command = $this->getApplication()->find('cache:clear');
        $command->run(
            new ArrayInput([]), $output
        );


        return Command::SUCCESS;
    }
}
