<?php

namespace App\Form;

use App\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'attr' => [
                    'placeholder' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vitae nunc et enim malesuada ',
                    'autocomplete'=>'off'
                ]
            ])
            ->add('excerpt', TextareaType::class, [
                'attr' => [
                    'placeholder' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vitae nunc et enim malesuada facilisis non id leo. Donec lectus tellus, tincidunt et luctus at, accumsan vitae arcu. Nullam diam purus, mollis vel orci non, rhoncus scelerisque urna. Donec vulputate risus molestie ipsum faucibus sodales. '
                ]
            ])
            ->add('cover', FileType::class, [
                'mapped' => false,
                'required' => !$options['edit'],
                'label' => $options['edit'] ? 'Cover image(leave blank if not changing)' : 'Cover image',
            ])
            ->add('body', TextareaType::class, [
                'attr' => [
                    'class' => 'ckeditor'
                ]
            ])
            ->add('public', CheckboxType::class, [
                'required' => false,
                'label' => 'Publish article?',
                
            ])
            ->add('submit', SubmitType::class, [
                'label' => $options['edit'] ? 'Save' : 'Post'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
            'edit' => false
        ]);
    }
}
