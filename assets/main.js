
// any CSS you import will output into a single css file (app.css in this case)
import './styles/main.scss';


const fileInputs = document.querySelectorAll('[type="file"]')

for (const input of fileInputs) {
    input.addEventListener('change', e=>{
        // change elementn content to filename
        const name = e.target.files[0]['name']
        const sibling = e.target.nextElementSibling.nextElementSibling

        // add input chlass
        e.target.classList.add('changed')
        console.log(sibling.innerHTML = name)
    },{passive: true})
}
